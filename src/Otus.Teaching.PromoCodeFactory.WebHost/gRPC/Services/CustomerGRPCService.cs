﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.gRPC.Services
{
    public class CustomerGRPCService : CustomersService.CustomersServiceBase
    {
        private IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerGRPCService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async override Task<CustomersResponse> GetCustomers(CustomersRequest request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = new CustomersResponse();

            response.Customers.Clear();
            response.Customers.Add(customers.Select(x => new CustomerGRPCResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList());

            return response;
        }

        public async override Task<CustomerGRPCResponse> GetCustomerById(CustomersByIdRequest request, ServerCallContext context)
        {
            var id = Guid.Parse(request.CustomerId);
            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerGRPCResponse()
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };

            response.Preferences.Clear();
            response.Preferences.Add(customer.Preferences
                    .Select(p => new PreferenceGRPCResponse()
                    {
                        Id = p.Preference.Id.ToString(),
                        Name = p.Preference.Name
                    })
                    .ToArray());

            return response;
        }

        public async override Task<CreateResponse> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(i => Guid.Parse(i)).ToList());

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return new CreateResponse() { Id = customer.Id.ToString() };
        }

        public async override Task<OkResponse> UpdateCustomer(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new ArgumentException("Not Found");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(i => Guid.Parse(i)).ToList());

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new OkResponse();
        }

        public async override Task<OkResponse> DeleteCustomer(DeleteCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.CustomerId));

            if (customer == null)
                throw new ArgumentException("Not Found");

            await _customerRepository.DeleteAsync(customer);

            return new OkResponse();
        }
    }
}
